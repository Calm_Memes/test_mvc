﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace WebApplication1.Models
{
    public class СomputerDbInitializer : DropCreateDatabaseAlways<ComputerContext>
    {
        protected override void Seed(ComputerContext db)
        {
            db.Computer.Add(new Computer
            {Name = "Acer", Country = "China", Price = 65000});
            db.Computer.Add(new Computer
            {Name = "Asus", Country = "China",Price = 58000});
            db.Computer.Add(new Computer
            {Name = "Apple",Country = "USA", Price= 150000});
            base.Seed(db);
        }
    }
}