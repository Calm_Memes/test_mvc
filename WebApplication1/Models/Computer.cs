﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication1.Models
{
    public class Computer
    {
        //Idишники
        public int Id { get; set; }

        public string Name { get; set; }

        public string Country { get; set; }

        public int Price { get; set; }
    }
}