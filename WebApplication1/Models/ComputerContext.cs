﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
//Без данной библиотеки ничего не заработает!!!
using System.Data.Entity;

namespace WebApplication1.Models
{
    //Унаследуем свойства от DbContext(для создания контекстов)
    public class ComputerContext : DbContext
    {
        //Получаем из БД нужные нам данные (например набор обьектов "компуктеры")
        public DbSet<Computer> Computer { get; set; }
        public DbSet<Purchase> Purchases { get; set; }
    }
}